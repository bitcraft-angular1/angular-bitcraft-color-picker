/* globals document angular $ template */
'use strict';

/** */
var BitcraftColorPickerController = function ($scope, $element) {
    this.$scope = $scope;
    this.$element = $element;
    this.boundOutsideClickListener = this.outsideClickListener.bind(this);
};

BitcraftColorPickerController.prototype.$onInit = function () {
    this.popupVisible = false;

    if (Array.isArray(this.standardPalette) === false) {
        this.standardPalette = [
            '#FFFFFF', '#CDEFEC', '#90E7F0', '#4EEEFF', '#03C7FF', '#02ABEA', '#206BFF', '#0000D4',
            '#A4A4A4', '#BBE7D8', '#7FFDFF', '#61E4FF', '#51AEFF', '#6487FF', '#8363D9', '#6814D4',
            '#BEBEBE', '#97EFCE', '#70FFC6', '#C0F3F3', '#C3C4F3', '#BD8FD9', '#BD50D9', '#9816D4',
            '#DCDEDE', '#ACFFC4', '#D7FFAE', '#F0E9C7', '#F0BED8', '#D97FD6', '#D94FB4', '#F20884',
            '#FCFFC1', '#F1FF79', '#FFFA98', '#FFD697', '#D97A94', '#D94C7B', '#FF0C6C', '#DF1B5A',
            '#FCF305', '#E6FF21', '#E2FC4C', '#FCD83D', '#FF8B1A', '#F0660F', '#FF0705', '#DD0806',
            '#27FC20', '#81FC22', '#D3F43C', '#BC9A05', '#C65D03', '#D83903', '#C30501', '#7A0403',
            '#07601A', '#008011', '#99BC2D', '#806D11', '#654B44', '#555657', '#2D2D2D', '#000000'
        ];
    }

    // $ctrl.colorPreview element is accessed through children traversal
    // because angular expression have not been interpolated yet
    this.colorInput = this.$element[0].children[1].children[0];
    this.colorPreview = this.$element[0].children[1].children[1].children[0];
    this.colorPreview.style.backgroundColor = this.ngModel;
};

BitcraftColorPickerController.prototype.outsideClickListener = function (event) {
    var self = this;

    if ($(event.target).closest('#color-picker-' + this.$scope.$id).length === 0) {
        self.togglePopup(false);
        document.removeEventListener('click', self.boundOutsideClickListener);
    }
};

BitcraftColorPickerController.prototype.initialize = function () {
    var self = this;

    this.colorInput = $('#color-picker-input-' + this.$scope.$id)[0];
    this.popup = $('#color-picker-' + this.$scope.$id)[0].children[2];
    this.jColorInput = $('#jquery-color-picker-container-' + this.$scope.$id + ' > input');
    this.colorPreview = $('#color-picker-preview-' + this.$scope.$id + ' > i')[0];
    this.tabsContent = $('#popup-tabs-content-' + this.$scope.$id)[0];

    this.jColorInput.wheelColorPicker({
        layout: 'block',
        format: 'css',
        autoFormat: true,
        autoResize: false,
        sliders: 'wv'
    });
    this.jColorInput.wheelColorPicker('setValue', this.ngModel, true);
    this.jColorInput.on('slidermove', function () {
        self.updateModel(self.jColorInput.wheelColorPicker('getValue'), true);
    });
    this.jColorInput.on('change', function () {
        self.updateModel(self.jColorInput.wheelColorPicker('getValue'), true);
    });

    // Prevents automatic resizing behaviour which causes issues on IE11
    this.jColorInput.data('jQWCP.instance').refreshWidget = Function.prototype;

    this.updateModel(this.ngModel);

    this.initialized = true;
};

BitcraftColorPickerController.prototype.updateModel = function (_newValue, preventColorWheelUpdate) {
    var newValue = _newValue;

    if (!newValue || newValue.length === 0) {
        newValue = this.colorInput.value;
    }

    if (!newValue || newValue.length === 0) {
        newValue = 'FFFFFF';
    }

    newValue = ((newValue[0] === '#' ? '' : '#') + newValue).toUpperCase();

    this.ngModelCtrl.$setViewValue(newValue);
    this.ngModelCtrl.$commitViewValue(newValue);
    this.colorInput.value = newValue;
    this.colorPreview.style.backgroundColor = newValue;

    this.colorInput.redComponent = parseInt(newValue.substring(1, 3), 16);
    this.colorInput.greenComponent = parseInt(newValue.substring(3, 5), 16);
    this.colorInput.blueComponent = parseInt(newValue.substring(5, 7), 16);

    if (!preventColorWheelUpdate && this.jColorInput) {
        this.jColorInput.wheelColorPicker('setValue', newValue);
    }
};

BitcraftColorPickerController.prototype.updateModelByRGBComponent = function () {
    var newValue = '#' +
        (this.colorInput.redComponent < 16 ? '0' : '') + this.colorInput.redComponent.toString(16) +
        (this.colorInput.greenComponent < 16 ? '0' : '') + this.colorInput.greenComponent.toString(16) +
        (this.colorInput.blueComponent < 16 ? '0' : '') + this.colorInput.blueComponent.toString(16);

    this.updateModel(newValue);
};

BitcraftColorPickerController.prototype.togglePopup = function (_showPopup) {
    var showPopup = _showPopup;
    var hiddenCSS = 'hidden';

    if (this.initialized === undefined) {
        this.initialize();
    }

    if (showPopup === undefined) {
        showPopup = !this.popupVisible;
    }

    this.popup.className = (showPopup ? this.popup.className.replace(new RegExp(hiddenCSS, 'g'), '') : this.popup.className + ' ' + hiddenCSS);

    this.popupVisible = showPopup;

    if (this.popupVisible) {
        document.addEventListener('click', this.boundOutsideClickListener);
    }
};

angular.module('bitcraft-color-picker', ['ui.bootstrap'])
    .component('bitcraftColorPicker', {
        template: template,
        require: {ngModelCtrl: '^ngModel'},
        controller: ['$scope', '$element', BitcraftColorPickerController],
        bindings: {
            ngModel: '=',
            ngChange: '&',
            standardPalette: '<'
        }
    });
