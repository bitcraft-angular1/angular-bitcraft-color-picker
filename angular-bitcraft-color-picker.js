(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/* globals document angular $ template */
'use strict';

var template = '' +
'<style>' +
'.bitcraft-color-picker .input-group-addon .picked-color {' +
'    display: inline-block;' +
'    height: 16px;' +
'    width: 16px;' +
'    vertical-align: text-top;' +
'}' +
'.bitcraft-color-picker > .input-group-addon {' +
'    cursor: pointer;' +
'    border-top-right-radius: 4px !important;' +
'    border-bottom-right-radius: 4px !important;' +
'}' +
'.bitcraft-color-picker .popup {' +
'    outline: none;' +
'    position: absolute;' +
'    top: 100%;' +
'    right: 0;' +
'    z-index: 100;' +
'    width: 300px;' +
'}' +
'.bitcraft-color-picker .popup .popup-tabs-content {' +
'    margin-top: 0px;' +
'}' +
'.bitcraft-color-picker .popup .nav {' +
'    padding-bottom: 15px;' +
'    display: flex;' +
'    justify-content: center;' +
'}' +
'.bitcraft-color-picker .popup .nav > * {' +
'    cursor: pointer;' +
'    margin-right: 15px;' +
'}' +
'.bitcraft-color-picker .popup .nav > *:last-child {' +
'    margin-right: 0;' +
'}' +
'.bitcraft-color-picker .popup .nav::before {' +
'    content:none;' +
'}' +
'.bitcraft-color-picker .jquery-color-picker-container {' +
'    display: flex;' +
'    justify-content: center;' +
'    border: 0;' +
'    box-shadow: none;' +
'    background-color: transparent;' +
'}' +
'.bitcraft-color-picker .popup .hidden {' +
'    opacity: 0;' +
'}' +
'.bitcraft-color-picker .jquery-color-picker-container > .jQWCP-wWidget.jQWCP-block {' +
'    outline: none;' +
'    border: none;' +
'    box-shadow: none;' +
'    background-color: transparent;' +
'    padding: 0 !important;' +
'}' +
'.bitcraft-color-picker .jQWCP-wWheelOverlay {' +
'    opacity: 0 !important;' +
'}' +
'.bitcraft-color-picker .standard-palette {' +
'    text-align: center;' +
'}' +
'.bitcraft-color-picker .standard-palette-choice {' +
'    user-select: none;' +
'    display: inline-flex;' +
'    width: 23px;' +
'    height: 23px;' +
'    border-radius: 50%;' +
'    margin: 5px;' +
'    box-shadow: 0.5px 3px #ccc;' +
'    cursor: pointer;' +
'}' +
'.bitcraft-color-picker .standard-palette-choice::after {' +
'    content: \'\';' +
'    display: block;' +
'    padding-bottom: 100%;' +
'}' +
'.bitcraft-color-picker .rgb-component-input {' +
'    margin: 10px 0 0 0;' +
'    text-align: center;' +
'}' +
'.bitcraft-color-picker .rgb-component-input .input-group {' +
'    max-width: 80px;' +
'    margin: 0 2px;' +
'}' +
'.bitcraft-color-picker .rgb-component-input .input-group .input-group-addon {' +
'    padding: 0 8px;' +
'    user-select: none;' +
'}' +
'.bitcraft-color-picker .rgb-component-input .input-group .form-control {' +
'    border-left-color: transparent;' +
'    padding-left: 4px;' +
'    padding-right: 5px;' +
'    text-align: right;' +
'}' +
'.bitcraft-color-picker .rgb-component-input .red {' +
'    color: #ffebee; /* 50 */' +
'    background-color: #f44336; /* 500 */' +
'    border-color: #c62828; /* 800 */' +
'}' +
'.bitcraft-color-picker .rgb-component-input .green {' +
'    color: #e8f5e9; /* 50 */' +
'    background-color: #4caf50; /* 500 */' +
'    border-color: #2e7d32; /* 800 */' +
'}' +
'.bitcraft-color-picker .rgb-component-input .blue {' +
'    color: #e3f2fd; /* 50 */' +
'    background-color: #2196f3; /* 500 */' +
'    border-color: #1565c0; /* 800 */' +
'}' +
'</style>' +
'<div id="{{\'color-picker-\' + $id}}" class="input-group bitcraft-color-picker">' +
'    <input id="{{\'color-picker-input-\' + $id}}" type="text" ng-model="$ctrl.ngModel" ng-change="$ctrl.updateModel()" class="form-control">' +
'    <span id="{{\'color-picker-preview-\' + $id}}" ng-click="$ctrl.togglePopup()" class="input-group-addon"><i class="picked-color" ng-style="{backgroundColor: \'#\' + ngModel}"></i></span>' +
'    <!-- TODO: reimplement using angular-bootstrap\'s popover component -->' +
'    <div class="popup panel panel-default hidden" tabindex="0" ng-blur="$ctrl.togglePopup(true)">' +
'        <div class="panel-body">' +
'            <uib-tabset active="activePill" vertical="true" type="pills">' +
'                <uib-tab index="0" heading="{{\'COLOR_PICKER.STANDARD_COLORS\' | translate}}">' +
'                    <div class="standard-palette">' +
'                        <div ng-repeat="color in $ctrl.standardPalette" ng-click="$ctrl.updateModel(color)" class="standard-palette-choice" ng-style="{backgroundColor: color}">&nbsp;</div>' +
'                    </div>' +
'                </uib-tab>' +
'                <uib-tab index="1" heading="{{\'COLOR_PICKER.CUSTOM_COLORS\' | translate}}">' +
'                    <div id="{{\'jquery-color-picker-container-\' + $parent.$parent.$id}}" class="jquery-color-picker-container">' +
'                        <input type="text" ng-model="ngModel" ng-change="updateModel()" class="form-control">' +
'                    </div>' +
'                    <div class="rgb-component-input">' +
'                        <div ng-repeat="colorComponent in [\'red\', \'green\', \'blue\']" class="input-group mb-3">' +
'                            <span class="input-group-addon {{colorComponent}}">{{colorComponent[0].toUpperCase()}}</span>' +
'                            <input class="form-control" type="number" min="0" max="255" ng-model="$ctrl.colorInput[colorComponent + \'Component\']" ng-change="$ctrl.updateModelByRGBComponent()">' +
'                        </div>' +
'                    </div>' +
'                </uib-tab>' +
'            </uib-tabset>' +
'        </div>' +
'    </div>' +
'</div>' +
'';


/** */
var BitcraftColorPickerController = function ($scope, $element) {
    this.$scope = $scope;
    this.$element = $element;
    this.boundOutsideClickListener = this.outsideClickListener.bind(this);
};

BitcraftColorPickerController.prototype.$onInit = function () {
    this.popupVisible = false;

    if (Array.isArray(this.standardPalette) === false) {
        this.standardPalette = [
            '#FFFFFF', '#CDEFEC', '#90E7F0', '#4EEEFF', '#03C7FF', '#02ABEA', '#206BFF', '#0000D4',
            '#A4A4A4', '#BBE7D8', '#7FFDFF', '#61E4FF', '#51AEFF', '#6487FF', '#8363D9', '#6814D4',
            '#BEBEBE', '#97EFCE', '#70FFC6', '#C0F3F3', '#C3C4F3', '#BD8FD9', '#BD50D9', '#9816D4',
            '#DCDEDE', '#ACFFC4', '#D7FFAE', '#F0E9C7', '#F0BED8', '#D97FD6', '#D94FB4', '#F20884',
            '#FCFFC1', '#F1FF79', '#FFFA98', '#FFD697', '#D97A94', '#D94C7B', '#FF0C6C', '#DF1B5A',
            '#FCF305', '#E6FF21', '#E2FC4C', '#FCD83D', '#FF8B1A', '#F0660F', '#FF0705', '#DD0806',
            '#27FC20', '#81FC22', '#D3F43C', '#BC9A05', '#C65D03', '#D83903', '#C30501', '#7A0403',
            '#07601A', '#008011', '#99BC2D', '#806D11', '#654B44', '#555657', '#2D2D2D', '#000000'
        ];
    }

    // $ctrl.colorPreview element is accessed through children traversal
    // because angular expression have not been interpolated yet
    this.colorInput = this.$element[0].children[1].children[0];
    this.colorPreview = this.$element[0].children[1].children[1].children[0];
    this.colorPreview.style.backgroundColor = this.ngModel;
};

BitcraftColorPickerController.prototype.outsideClickListener = function (event) {
    var self = this;

    if ($(event.target).closest('#color-picker-' + this.$scope.$id).length === 0) {
        self.togglePopup(false);
        document.removeEventListener('click', self.boundOutsideClickListener);
    }
};

BitcraftColorPickerController.prototype.initialize = function () {
    var self = this;

    this.colorInput = $('#color-picker-input-' + this.$scope.$id)[0];
    this.popup = $('#color-picker-' + this.$scope.$id)[0].children[2];
    this.jColorInput = $('#jquery-color-picker-container-' + this.$scope.$id + ' > input');
    this.colorPreview = $('#color-picker-preview-' + this.$scope.$id + ' > i')[0];
    this.tabsContent = $('#popup-tabs-content-' + this.$scope.$id)[0];

    this.jColorInput.wheelColorPicker({
        layout: 'block',
        format: 'css',
        autoFormat: true,
        autoResize: false,
        sliders: 'wv'
    });
    this.jColorInput.wheelColorPicker('setValue', this.ngModel, true);
    this.jColorInput.on('slidermove', function () {
        self.updateModel(self.jColorInput.wheelColorPicker('getValue'), true);
    });
    this.jColorInput.on('change', function () {
        self.updateModel(self.jColorInput.wheelColorPicker('getValue'), true);
    });

    // Prevents automatic resizing behaviour which causes issues on IE11
    this.jColorInput.data('jQWCP.instance').refreshWidget = Function.prototype;

    this.updateModel(this.ngModel);

    this.initialized = true;
};

BitcraftColorPickerController.prototype.updateModel = function (_newValue, preventColorWheelUpdate) {
    var newValue = _newValue;

    if (!newValue || newValue.length === 0) {
        newValue = this.colorInput.value;
    }

    if (!newValue || newValue.length === 0) {
        newValue = 'FFFFFF';
    }

    newValue = ((newValue[0] === '#' ? '' : '#') + newValue).toUpperCase();

    this.ngModelCtrl.$setViewValue(newValue);
    this.ngModelCtrl.$commitViewValue(newValue);
    this.colorInput.value = newValue;
    this.colorPreview.style.backgroundColor = newValue;

    this.colorInput.redComponent = parseInt(newValue.substring(1, 3), 16);
    this.colorInput.greenComponent = parseInt(newValue.substring(3, 5), 16);
    this.colorInput.blueComponent = parseInt(newValue.substring(5, 7), 16);

    if (!preventColorWheelUpdate && this.jColorInput) {
        this.jColorInput.wheelColorPicker('setValue', newValue);
    }
};

BitcraftColorPickerController.prototype.updateModelByRGBComponent = function () {
    var newValue = '#' +
        (this.colorInput.redComponent < 16 ? '0' : '') + this.colorInput.redComponent.toString(16) +
        (this.colorInput.greenComponent < 16 ? '0' : '') + this.colorInput.greenComponent.toString(16) +
        (this.colorInput.blueComponent < 16 ? '0' : '') + this.colorInput.blueComponent.toString(16);

    this.updateModel(newValue);
};

BitcraftColorPickerController.prototype.togglePopup = function (_showPopup) {
    var showPopup = _showPopup;
    var hiddenCSS = 'hidden';

    if (this.initialized === undefined) {
        this.initialize();
    }

    if (showPopup === undefined) {
        showPopup = !this.popupVisible;
    }

    this.popup.className = (showPopup ? this.popup.className.replace(new RegExp(hiddenCSS, 'g'), '') : this.popup.className + ' ' + hiddenCSS);

    this.popupVisible = showPopup;

    if (this.popupVisible) {
        document.addEventListener('click', this.boundOutsideClickListener);
    }
};

angular.module('bitcraft-color-picker', ['ui.bootstrap'])
    .component('bitcraftColorPicker', {
        template: template,
        require: {ngModelCtrl: '^ngModel'},
        controller: ['$scope', '$element', BitcraftColorPickerController],
        bindings: {
            ngModel: '=',
            ngChange: '&',
            standardPalette: '<'
        }
    });

},{}],2:[function(require,module,exports){
/*global angular*/
'use strict';
require('./bitcraft-color-picker.component.js');

},{"./bitcraft-color-picker.component.js":1}]},{},[2]);
